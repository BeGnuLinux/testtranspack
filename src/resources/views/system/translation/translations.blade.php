@extends('admin.master-settings')

@section('pageCsCode')
    <link href="{{URL('admin-themes/assets/plugins/custom/jstree/jstree.bundle.css')}}" rel="stylesheet" type="text/css" />
@stop

@section('pageContent')

    <div class="row">

        <div class="col-12 mt-3 mb-3">
            <div class="row">
                <div class="col-lg-1">
                    <h2>
                        {{__("admin.system.translations.pageTitle")}}
                    </h2>
                </div>
                <div class="col-lg-11 d-print-none">
                    <div class="pull-right">
						<a href="{{route('translations.scan')}}" class="btn btn-label-brand btn-bold">
							<i class="fas fa-search"></i> {{__("admin.system.translations.Scan translations")}}
						</a>
						<a href="{{route('translations.publish')}}" class="btn btn-default btn-bold">
							<i class="fas fa-upload"></i> {{__("admin.system.translations.Publish translations")}}
						</a>
						<a onclick="ajaxSubmit('{{route('translations.unpublished')}}','formData','GET')" class="btn btn-warning btn-bold">
							{{__("admin.system.translations.Unpublished items")}}&nbsp;
							<span class="badge badge-light ">
                                {{@$unpublishedItems}}
                            </span>
						</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="row">
                <div class="col-lg-3">
                    <div class="kt-portlet d-flex h-100 flex-column">
                        <div class="kt-portlet__body">
                            <div id="kt_tree_translation" class="tree-demo">
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-lg-9">
					<div class="col-12 d-print-none mb-3">
						<div class="accordion accordion-outline" id="accordion_filter">
							<div class="card">
								<div class="card-header" id="heading_filter">
									<div class="card-title {{@$filter->search!="1" ? "collapsed" : ""}}" data-toggle="collapse" data-target="#collapse_filter" aria-expanded="{{@$filter->search!="1" ? "false" : "true"}}" aria-controls="collapse_filter">
										{{__("admin.system.translations.Search")}}
									</div>
								</div>
								<div id="collapse_filter" class="card-body-wrapper {{@$filter->search!="1" ? "collapse" : "show"}}" aria-labelledby="heading_filter" data-parent="#accordion_filter" style="">
									<div class="card-body">
										<form class="kt-form" method="GET" name="SearchForm" id="SearchForm">
											<input type="hidden" name="search" value="1">
											<div class="form-group row">
												<div class="col-lg-2">
													<label for="site_id">{{__("admin.system.translations.Site")}}:</label>
													<select name="site_id" id="site_id" class="custom-select">
														<option value="">
															{{__("admin.system.translations.Please select")}}
														</option>
														@foreach(\Ecms\Translate\Models\Admin\Lookup\System\Site\Site::all() as $lookupItem)
															<option value="{{$lookupItem->id}}" {{old('site_id', @$filter->site_id) === $lookupItem->id ? "selected" : "" }}>
																{{$lookupItem->site_title}} - ({{$lookupItem->site_language}})
															</option>
														@endforeach
													</select>
												</div>

												<div class="col-lg-8">
													<label for="find_in_field">{{__("admin.system.translations.Find in")}}:</label>
													<div class="input-group">
														<select name="find_in_field" id="find_in_field" class="custom-select col-lg-3">
															<option value="translation_key" {{@$filter->find_in_field=="translation_key" ? "selected" : ""}}>
																{{__("admin.system.translations.Translation key")}}
															</option>

															<option value="translations_raw" {{@$filter->find_in_field=="translations" ? "selected" : ""}}>
																{{__("admin.system.translations.Translation")}}
															</option>
														</select>
														<select name="find_in_operator" id="find_in_operator" class="custom-select col-lg-3">
															<option value="contain" {{@$filter->find_in_operator=="contain" ? "selected" : ""}}>
																{{__('admin.system.translations.Contain')}}
															</option>
															<option value="equal_to" {{@$filter->find_in_operator=="equal_to" ? "selected" : ""}}>
																{{__('admin.system.translations.Equal to')}}
															</option>
															<option value="start_with" {{@$filter->find_in_operator=="start_with" ? "selected" : ""}}>
																{{__('admin.system.translations.Start with')}}
															</option>
															<option value="end_with" {{@$filter->find_in_operator=="end_with" ? "selected" : ""}}>
																{{__('admin.system.translations.End with')}}
															</option>
														</select>
														<input type="text" name="find_in_value" id="find_in_value" maxlength="255" value="{{@$filter->find_in_value}}" class="form-control col-lg-6" aria-describedby="" placeholder="">
													</div>
												</div>

												<div class="col-lg-2">
													<label for="record_state">{{__("admin.system.translations.Published")}}:</label>
													<div class="input-group">
														<select name="translation_published" id="translation_published" class="custom-select">
															<option value="">
																{{__("admin.system.translations.All")}}
															</option>
															<option value="0" {{@$filter->translation_published=="0" ? "selected" : ""}}>
																{{__("admin.system.translations.No")}}
															</option>
															<option value="1" {{@$filter->translation_published=="1" ? "selected" : ""}}>
																{{__("admin.system.translations.Yes")}}
															</option>
														</select>
													</div>
												</div>

											</div>
											<a onclick="postForm('{{route('translations.search')}}'); return false;" class="btn btn-label btn-label-brand btn-bold">
												<i class="fas fa-search"></i> {{__("admin.system.translations.Search")}}
											</a>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="formData"  class="col-12">

					</div>
				</div>
            </div>

        </div>
    </div>
    
@stop

@section('pageJsCode')

    <script type="text/javascript">
		"use strict";
		jQuery(document).ready(function() {
			$("#kt_tree_translation").jstree({
				"core" : {
					'data' : {
						'url' : function (node) {
							return '{{route('jsonTranslation')}}';
						},
						'data' : function (node) {
							return { 'parent' : node.id };
						}
					},
					"themes" : {
						"responsive": false
					},
					"check_callback" : true,
				},
				"state" : { "key" : "{{Request::url()}}" },
				"themes" : {
					"responsive": true
				},
				"plugins" : ["wholerow","state"]
			});
		});

		$('#kt_tree_translation').on("select_node.jstree", function (e, data) {
			ajaxSubmit('{{route('translations-item')}}/' + data.node.id,'formData','GET')
		});

    </script>

	<script type="text/javascript">
		function updateTranslation($div,$id,$lang) {

			var formData = new FormData();
			formData.append('id', $id);
			formData.append('lang', $lang);
			formData.append('text', $div.value);

			$.ajax({
				url: '{{route('translations.update')}}',
				type: 'POST',
				dataType: 'JSON',
				headers: {'X-CSRF-TOKEN': '{{csrf_token()}}'},
				data: formData,
				contentType: false,
				processData: false,
				success: function (data) {
					console.log(data);
				},
				error: function (data, textStatus, errorThrown) {
					console.log(data);
				}
			});
		}

		function postForm(path) {
			form = document.getElementById("SearchForm");

			$url = '{{route('translations.search')}}/?site_id=' + form.site_id.value
			+ '&translation_published=' + form.translation_published.value
			+ '&find_in_value=' + form.find_in_value.value
			+ '&find_in_field=' + form.find_in_field.value
			+ '&find_in_operator=' + form.find_in_operator.value;

			ajaxSubmit($url,'formData','GET')
		}

		function googleTranslation($div,$id,$lang,$str,defaultLang = false) {
			var obj=document.getElementById($div);

			if(defaultLang == false){
				$.get('https://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl='+$lang+'&dt=t&q='+$str, function(data, status){
					obj.value=data[0][0][0];
					updateTranslation(obj,$id,$lang);
				});
			} else {
				obj.value = $str;
				updateTranslation(obj,$id,$lang);
			}
		}
	</script>
@stop

