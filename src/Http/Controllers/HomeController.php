<?php

namespace Ecms\Translate\Http\Controllers;

use App\Http\Controllers\Controller;

class HomeController extends Controller {
	
	public function __construct() {
		
//		$this->middleware('auth');
	}
	
	public function index() {
		
		$result = array();
		
		return view('admin.home')->with($result);
		
	}
	
	public function settings() {
		$result = array();
		
		return view('admin.home-settings')->with($result);
	}
	
}
