<?php

namespace Ecms\Translate\Models\Admin\System\Site;

use Ecms\Translate\Models\BaseModel;
use Ecms\Translate\Models\Triggers\AutoGuid;
use Ecms\Translate\Models\Triggers\CreatedBy;
use Ecms\Translate\Models\Triggers\UpdatedBy;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Site extends BaseModel {
	
	use SoftDeletes, CreatedBy, UpdatedBy, AutoGuid;
	
	protected $fillable = [
		'site_title',
		'site_lang',
		'site_language',
		'site_direction',
		'translation_required',
		'site_logo',
		'site_flag',
		'configurations',
		'translations',
		'language_id',
		'country_id',
		'default_site',
		'record_priority',
		'record_state',
		'cache_state',
	];
	
	protected static function boot() {
		parent::boot();
		
		static::addGlobalScope('module', function(Builder $builder) {
			$builder->where(function($query) {
				$query->whereNull('domain')->orWhere('domain', env('APP_DOMAIN'));
			});
		});
		
		// Order by id desc
		static::addGlobalScope('order', function(Builder $builder) {
			$builder->orderBy('id', 'desc');
		});
		
		static::creating(function($model) {
			$model->domain = env('APP_DOMAIN');
		});
	}
	
	public function getCreatedAtAttribute($value) {
		return Carbon::parse($value)->format(__('admin.DATE_FORMAT'));
	}
	
	public function getRecordStateAttribute($value) {
		return $value == 0 && !is_null($value) ? false : true;
	}
	
	public function getStatusAttribute($value) {
		return $this->record_state==1  ? __("admin.content.faq.Active") : __("admin.content.faq.Disabled");
	}
	
	public function getStatusColorAttribute($value) {
		return $this->record_state==1  ? "success" : "secondary";
	}
	
	public function getPublishAtAttribute($value) {
		return $value ? Carbon::parse($value)->format(__('admin.DATE_FORMAT')) : null;
	}
	
	public function getExpireAtAttribute($value) {
		return $value ? Carbon::parse($value)->format(__('admin.DATE_FORMAT')) : null;
	}
}
