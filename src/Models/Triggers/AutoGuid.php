<?php

namespace Ecms\Translate\Models\Triggers;

use Illuminate\Support\Str;

trait AutoGuid {
	protected static function bootAutoGuid() {
		static::creating(function($model) {
			$model->guid = strtolower(Str::uuid());
		});
	}
	
	public function getKeyType() {
		return 'string';
	}
}

